print('=== hello ===')

###############
### IMPORTS ###
###############

from io import open
import glob
import os
import string
import torch
import torch.nn as nn
from anyascii import anyascii as aa
import random
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pathlib

print('--- imports done ---')

###################
### DIAGNOSTICS ###
###################
print()
print('Cuda available: ' + str(torch.cuda.is_available()))
print('Cuda device coint: ' + str(torch.cuda.device_count()))
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('Using device:', device)
if device.type == 'cuda':
	torch.set_default_tensor_type('torch.cuda.FloatTensor')
	print(torch.cuda.get_device_name(0))
	print('Memory Usage:')
	print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
	print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')
print()

#######################
### DATA PROCESSING ###
#######################
# Define data processing functions and constants

all_letters = string.printable
n_letters = len(all_letters)

def letterToIndex(letter):
	return all_letters.find(letter)

def indexToLetter(index):
	return all_letters[index]

# Just for demonstration, turn a letter into a <1 x n_letters> Tensor
def letterToTensor(letter):
	tensor = torch.zeros(1, n_letters)
	tensor[0][letterToIndex(letter)] = 1
	return tensor

def textToTensor(text):
	tensor = torch.zeros(len(text), 1, n_letters)
	for li, letter in enumerate(text):
		tensor[li][0][letterToIndex(letter)] = 1
	return tensor

def tensorToLetter(tensor):
	return indexToLetter(tensor.argmax())

################
### SAMPLING ###
################

def randomChoice(l):
	return l[random.randint(0, len(l) - 1)]

#def randomTrainingExample():
#	category = randomChoice(all_categories)
#	line = randomChoice(category_lines[category])
#	category_tensor = torch.tensor([all_categories.index(category)], dtype=torch.long)
#	line_tensor = lineToTensor(line)
#	return category, line, category_tensor, line_tensor

#for i in range(10):
#	category, line, category_tensor, line_tensor = randomTrainingExample()
#	print('category =', category, '/ line =', line)

##########################
### DEFINE THE NETWORK ###
##########################

class RNN(nn.Module):
	def __init__(self, input_size, hidden_size, output_size):
		super(RNN, self).__init__()

		self.hidden_size = hidden_size
		self.h = torch.zeros(1, self.hidden_size)
		self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
		self.h2o = nn.Linear(hidden_size, output_size)
		self.nonlin = nn.Tanh()

	def forward(self, input):
		combined = torch.cat((input, self.h), 1)
		self.h = self.i2h(combined)
		output = self.h2o(self.h)
		output = self.nonlin(output)
		return output

	def resetHidden(self):
		self.h = torch.zeros(1, self.hidden_size)

class RNNStack(nn.Module):
	def __init__(self, input_size, hidden_size, output_size, layers=1):
		super(RNNStack, self).__init__()

		self.layers = nn.ParameterList()
		self.layers.append(RNN(input_size, hidden_size, hidden_size))
		for _ in range(layers):
			self.layers.append(RNN(hidden_size, hidden_size, hidden_size))
		self.layers.append(RNN(hidden_size, hidden_size, output_size))

		self.softmax = nn.Softmax(dim=1)

	def forward(self, input):
		y = input
		for l in self.layers:
			y = l(y)
		y = self.softmax(y)
		return y

	def resetHidden(self):
		for l in self.layers:
			l.resetHidden()

n_hidden = 64
rnn = RNNStack(n_letters, n_hidden, n_letters, 3)

print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

print('Parameters: ', count_parameters(rnn))

# Simple prediction
def predict(input, to_predict=32):
	input = textToTensor(input)
	# Reset hidden state
	rnn.resetHidden()
	last = None
	for i in input[:-1]:
		last = rnn(i)
	output = ''
	for _ in range(to_predict):
		last = rnn(last)
		output += tensorToLetter(last)
	return output

################
### TRAINING ###
################
learning_rate = 0.00025
loss_fn = nn.CrossEntropyLoss()
# optimizer = torch.optim.SGD(rnn.parameters(), lr=learning_rate) # Use for verification
optimizer = torch.optim.Adam(rnn.parameters(), lr=learning_rate) # Use for training (TODO: Try to use the Sophia optimizer instead)

def train(prompt_tensor):
	# Set the model to training mode - important for batch normalization and dropout layers
	# Unnecessary in this situation but added for best practices
	rnn.train()

	# Reset the hidden state (we're an RNN)
	rnn.resetHidden()

	# Make a prediction (for a single sample)
	output = rnn(prompt_tensor[0])
	loss = loss_fn(output.flatten(), prompt_tensor[1].flatten())
	for i in range(prompt_tensor.size()[0] - 2):
		output = rnn(prompt_tensor[i + 1])
		loss += loss_fn(output.flatten(), prompt_tensor[i + 2].flatten())

	# Backprop...
	loss.backward()
	torch.nn.utils.clip_grad_norm_(rnn.parameters(), 1)
	optimizer.step()
	optimizer.zero_grad()

	# if batch % 100 == 0:
		# loss, current = loss.item(), (batch + 1) * len(X)
		# print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

	return loss.item()

print('=== reading inputs ===')

with open(str(pathlib.Path(__file__).parent.resolve()) + '/data/shakespeare.txt') as file:
	txt = file.read()
training_tensor = textToTensor(txt)
# print(txt)
# print(training_tensor)
print('=== reading finished ===')

SEQUENCE_LENGTH = 64
for x in range(len(training_tensor) - SEQUENCE_LENGTH - 1):
	loss = train(training_tensor[x:x+SEQUENCE_LENGTH+2])
	if x % 100 == 10:
		print(loss)
		prompt = "BENVOLIO:\nHe ran this way, and leap'd this "
		print('---')
		print(prompt)
		print('---')
		print('|' + predict(prompt) + '|')
		print('---')


##########################
### PLOTS BECAUSE QOOL ###
##########################
# plt.figure()
# plt.plot(all_losses)

print('=== all done ===')
