use std::ops::{AddAssign, MulAssign};

use nalgebra::{self, DMatrix};

type V = f32;
pub type Matrix = DMatrix<V>;

struct GPT {}

pub fn ones(nrows: usize, ncols: usize) -> Matrix {
    Matrix::from_fn(nrows, ncols, |_, _| 1.)
}

pub fn zeros(nrows: usize, ncols: usize) -> Matrix {
    Matrix::from_fn(nrows, ncols, |_, _| 0.)
}

impl GPT {
    pub fn new(heads: usize) -> Self {
        //
        GPT {}
    }
}

pub fn linear(input: &Matrix, weights: &Matrix, bias: &Matrix) -> Matrix {
    input * weights + bias
}

const PI: V = 3.1415;

// Applies gelu to a matrix
pub fn gelu(x: &mut Matrix) {
    x.apply(|x| {
        *x = 0.5 * *x * (1. + ((2. / PI).sqrt() * (*x + 0.044715 * (*x).powi(3))).tanh());
    });
}

// Applies relu to a matrix
pub fn relu(x: &mut Matrix) {
    x.apply(|x| {
        *x = (*x).max(0.);
    });
}

pub fn softmax(x: &mut Matrix) {
    for mut row in x.row_iter_mut() {
        let max = row.max();
        row.add_scalar_mut(-max);
    }
    x.apply(|x| *x = (*x).exp());
    for mut row in x.row_iter_mut() {
        let sum = row.sum();
        row.mul_assign(1.0 / sum);
    }
}

pub fn layer_norm(x: &mut Matrix, gamma: &Matrix, beta: &Matrix, eps: Option<f32>) {
    let eps = if let Some(eps) = eps { eps } else { 1e-5 };
    for mut row in x.row_iter_mut() {
        let mean = row.mean();
        let variance = row.variance();
        row.add_scalar_mut(-mean);
        row.mul_assign(1.0 / (variance + eps).sqrt());
    }
    // after getting a mean of 0 and variance of 1, scale and offset
    for mut row in x.row_iter_mut() {
        row.component_mul_assign(gamma);
        row.add_assign(beta);
    }
}

fn gpt2() {
    // token + positional embeddings

    // forward pass through n_layer transformer blocks

    // projection to vocab
}
