use bincode::de;

pub fn mean_square(prediction: &[f32], target: &[f32], error: &mut [f32]) -> f32 {
    debug_assert!(
        prediction.len() == target.len(),
        "Prediction and target vectors must be equal!"
    );
    debug_assert!(prediction.len() > 0, "Predictions must be non empty!");

    let mut loss = 0.;
    for t in prediction.iter().zip(target).enumerate() {
        let delta = t.1 .0 - t.1 .1;
        loss += delta * delta;
        error[t.0] += delta;
    }

    loss / prediction.len() as f32
}
