pub mod annealing;
pub mod data;
pub mod dfa;
pub mod gpt;
pub mod loss;
pub mod post_processing;
pub mod spiking;
pub mod testing;
pub mod tests;
pub mod tokenizer;

use anyhow::Result;
use std::time::Instant;

use crate::annealing::Annealing;
use crate::spiking::{LayerType, NeuronLayer};

// Global config
pub const BERT: bool = false;
pub const TOKENIZER: &str = "bert-base-cased";

pub fn get_vocab_size() -> usize {
    if BERT {
        tokenizers::tokenizer::Tokenizer::from_pretrained(TOKENIZER, None)
            .unwrap()
            .get_vocab_size(true)
    } else {
        95
    }
}

// Prints a borrowed vector with up to 4 decimal places of precision
pub fn pretty_print_vector(v: &Vec<f32>) {
    println!("{{");
    for f in v {
        print!("{:.4}, ", f);
    }
    println!("\n}}");
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct UsizeDim {
    pub size: usize,
}
unsafe impl nalgebra::Dim for UsizeDim {
    fn try_to_usize() -> Option<usize> {
        None
    }

    fn value(&self) -> usize {
        self.size
    }

    fn from_usize(dim: usize) -> Self {
        Self { size: dim }
    }
}
impl PartialOrd for UsizeDim {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.size.partial_cmp(&other.size)
    }
}

pub fn main() -> Result<()> {
    let vocab = get_vocab_size();
    println!("Vocab size: {}", vocab);
    println!("===");
    let trained = data::file_to_text_training_vector("./data/shakespeare-copy.txt".into());
    println!("===");

    let inputs: usize = 64; // also, embedding size
    const HIDDEN: usize = 128;
    let outputs: usize = vocab;
    const SEQUENCE_SIZE: usize = 32;

    let mut nn = spiking::Net::new(inputs, outputs, vocab, 0);
    // Make a "previous state layer" holding hidden state data for recursion
    nn.add_layer(NeuronLayer::typed(LayerType::RNN, inputs, HIDDEN));
    nn.add_layer(NeuronLayer::typed(LayerType::LeakyRelu, HIDDEN, HIDDEN));
    nn.add_layer(NeuronLayer::typed(LayerType::RNN, HIDDEN, HIDDEN));
    nn.add_layer(NeuronLayer::typed(LayerType::LeakyRelu, HIDDEN, outputs));

    nn.reseed();

    // Log out some debugging info
    nn.print_params();

    // Train
    let mut best = nn.clone();
    let mut best_score = f32::MAX;
    // Try up to 5 times...
    for _attempts in 0..1 {
        let mut nn = best.clone();
        nn.reseed();

        println!("===");

        let start_time = std::time::Instant::now();
        const ANNEALING_STEPS: usize = 300000000;
        let mut rolling_loss = 0.;
        let mut rolling_loss_long = 0.;
        let mut rolling_loss_glac = 0.;
        let mut rolling_anneal_time = 0.;
        for i in 0..ANNEALING_STEPS {
            let start = i % (trained.len() - 2 * SEQUENCE_SIZE);
            let instant_now = Instant::now();
            use rayon::prelude::*;
            let (sender, receiver) = std::sync::mpsc::sync_channel(128);
            (0..32).into_par_iter().for_each(|_| {
                //
                let mut cloned = best.clone();
                let mut mirrored = best.clone();
                let mut annealer: Annealing = annealing::annealing(0.99999999, 0.0001);
                let loss = annealer.anneal_on_text(
                    &mut cloned,
                    &mut mirrored,
                    &trained,
                    start,
                    SEQUENCE_SIZE,
                );
                sender.send((cloned, loss)).unwrap();
            });

            while let Ok((candidate, loss)) = receiver.try_recv() {
                //
                if loss < best_score {
                    best_score = loss;
                    best = candidate;
                }
            }
            let loss = best_score;

            let time = Instant::now() - instant_now;
            if i == 0 {
                rolling_anneal_time = time.as_secs_f64();
                rolling_loss = loss as f64;
                rolling_loss_long = loss as f64;
                rolling_loss_glac = loss as f64;
            }
            rolling_anneal_time = 0.99 * rolling_anneal_time + 0.01 * time.as_secs_f64();
            rolling_loss = 0.99 * rolling_loss + 0.01 * loss as f64;
            rolling_loss_long = 0.9999 * rolling_loss_long + 0.0001 * loss as f64;
            rolling_loss_glac = 0.999999 * rolling_loss_glac + 0.000001 * loss as f64;

            if i % 100 == 10 {
                let prompt = &trained[start..start + SEQUENCE_SIZE].to_vec();
                let pred = Annealing::predict_text(&mut best, prompt, SEQUENCE_SIZE, HIDDEN);
                if BERT {
                    let tk =
                        tokenizers::tokenizer::Tokenizer::from_pretrained(TOKENIZER, None).unwrap();
                    println!(
                        "{:?}\n{:?}",
                        prompt,
                        tk.decode(prompt.clone(), true).unwrap()
                    );
                    println!("{:?}\n{:?}", pred, tk.decode(pred.clone(), true).unwrap());
                } else {
                    let tk = tokenizer::Tokenizer::new();
                    println!("{:?}\n{:?}", prompt, tk.tokens_to_string(prompt));
                    println!("{:?}\n{:?}", pred, tk.tokens_to_string(&pred));
                }
                println!(
                    "Time: {}\nRolling Loss Glac: {}\nRolling Loss Long: {}\nRolling Loss:      {}\nLoss:              {}\nProgress: {}% [{}]     Temperature: {}\nAnnealer current failures: {},     Annealer success rate: {:.15}%\nETA: {}d   ({}h)   [{}m]",
                    rolling_anneal_time,
                    rolling_loss_glac,
                    rolling_loss_long,
                    rolling_loss,
                    loss,
                    i as f32 / ANNEALING_STEPS as f32 * 100., // progress
                    i,
					0, 0, 0,
                    ANNEALING_STEPS as f32 / i as f32 * (std::time::Instant::now() - start_time).as_secs_f32() / 60. / 60. / 24.,
                    ANNEALING_STEPS as f32 / i as f32 * (std::time::Instant::now() - start_time).as_secs_f32() / 60. / 60.,
                    ANNEALING_STEPS as f32 / i as f32 * (std::time::Instant::now() - start_time).as_secs_f32() / 60.
                );
            }
        }
        let end = std::time::Instant::now();
        let loss = Annealing::eval_on_text(&mut best, &trained, 0, SEQUENCE_SIZE);
        println!("=== final ===");
        println!("Loss: {}", loss);
        println!("Annealing time: {}", (end - start_time).as_secs_f64());
    }

    return Ok(());
}
