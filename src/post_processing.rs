use rayon::prelude::{IntoParallelRefMutIterator, ParallelIterator};

pub fn softmax(buffer: &mut [f32]) {
    // Softmaxify before returning
    let mut sum = 0.;
    const SOFTMAX_MIN: f32 = 0.000001;
    for o in buffer.iter() {
        sum += (*o).exp().max(SOFTMAX_MIN);
    }
    buffer.par_iter_mut().for_each(|o| {
        *o = (*o).exp().max(SOFTMAX_MIN) / sum;
    });
}
