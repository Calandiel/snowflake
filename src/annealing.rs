use nanorand::{Rng, WyRand};

use crate::{loss, spiking::Net};

const BATCH_SIZE: usize = 1;

#[derive(Debug)]
pub struct RandWy {
    pub wyrand: WyRand,
}
impl RandWy {
    pub fn new() -> Self {
        RandWy {
            wyrand: WyRand::new(),
        }
    }
}

impl rand_core::RngCore for RandWy {
    fn next_u32(&mut self) -> u32 {
        self.wyrand.generate()
    }

    fn next_u64(&mut self) -> u64 {
        self.wyrand.generate()
    }

    fn fill_bytes(&mut self, dest: &mut [u8]) {
        for x in dest {
            *x = self.wyrand.generate();
        }
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand_core::Error> {
        for x in dest {
            *x = self.wyrand.generate();
        }
        Ok(())
    }
}

#[derive(Default, Debug, Copy, Clone)]
pub struct Annealing {
    pub temperature: f64,
    pub cooling_rate: f64,
    pub failures: usize, // consecutive failures to improve the score
    pub diagnostic_total_failures: usize,
    pub diagnostic_total_anneals: usize,
    pub step_counter: usize,
    pub learning_rate: f64,
}

pub fn annealing(cooling_rate: f64, learning_rate: f64) -> Annealing {
    Annealing {
        temperature: 1.,
        cooling_rate,
        failures: 0,
        diagnostic_total_anneals: 0,
        diagnostic_total_failures: 0,
        step_counter: 0,
        learning_rate,
    }
}

impl Annealing {
    pub fn anneal_on_text(
        &mut self,
        original_network: &mut Net,
        cloned_network: &mut Net,
        text: &Vec<u32>,
        start: usize,
        size: usize,
    ) -> f32 {
        assert!(text.len() > 0, "Empty string!");

        let mut error_vec = vec![0.; original_network.get_outputs_count()];

        // Calculate loss on the based network for comparison
        let initial_loss =
            Self::eval_on_text_internal(original_network, text, start, size, &mut error_vec);
        //self.ssdfa(&mut (**original_network).borrow_mut(), &error_vec);
        //*
        // try to increase the loss up to a given amount of times...
        for _ in 0..10 {
            // Make a copy of the original network and perturb it
            original_network.write_to(cloned_network);
            self.perturb(cloned_network);

            // Evaluate loss on the cloned network
            let cloned_loss = Self::eval_on_text(
                cloned_network,
                text,
                start,
                size,
                // &mut error_vec,
            );

            let original_cloned_loss = cloned_loss;
            // TODO: Decide if this is worth it (scaling by spike count)
            // let cloned_loss = cloned_loss * (cloned_spikes as f32 / initial_spikes as f32 / 2. + 0.5);

            // Annealing requires us to perturb the model
            // println!("{} vs {}", cloned_loss, initial_loss);
            //let p_accept = (-(cloned_loss - initial_loss) / (50. * self.temperature as f32)).exp();
            //let mut rng = WyRand::new();

            self.diagnostic_total_anneals += 1;
            self.step_counter += 1;
            self.temperature *= self.cooling_rate;

            // If we improve on the best loss, do the mario :sunglasses:
            if cloned_loss <= initial_loss {
                //|| rng.generate::<f32>() < p_accept {
                // Swap 'em around!
                let old = original_network.clone();
                *original_network = cloned_network.clone();
                *cloned_network = old;
                self.failures = 0;
            } else {
                self.failures += 1;
                self.diagnostic_total_failures += 1;
            }

            // println!("Temperature: {}", self.temperature);
            if cloned_loss < initial_loss {
                return original_cloned_loss;
            }
        }
        //*/
        initial_loss
    }

    // Perturbs a neural network a bit
    fn perturb(&self, net: &mut Net) {
        net.perturb(self.learning_rate);
    }

    pub fn eval_on_text(net: &mut Net, input_vector: &Vec<u32>, start: usize, size: usize) -> f32 {
        let mut error_vec = vec![0.; net.get_outputs_count()];
        Self::eval_on_text_internal(net, input_vector, start, size, &mut error_vec)
    }
    pub fn eval_on_text_internal(
        net: &mut Net,
        input_vector: &Vec<u32>,
        start: usize,
        size: usize,
        error_vec: &mut Vec<f32>,
    ) -> f32 {
        for a in error_vec.iter_mut() {
            *a = 0.;
        }
        let mut loss = 0.;
        for ii in 0..BATCH_SIZE {
            let start = (start + ii * 100) % (input_vector.len() - size - 1);
            net.prepare_for_next_sequence();

            // Construct new inputs
            let mut outputs = vec![0.; net.get_outputs_count()];

            for offset in 0..size - 1 {
                // Run predictions
                let current = input_vector[start + offset];
                let prediction = input_vector[start + offset + 1];
                net.evaluate(current, &mut outputs);

                let mut target_vector = vec![0.; outputs.len()];
                target_vector[prediction as usize] = 1.;
                // if offset == size - 2 {
                loss += loss::mean_square(&outputs, &target_vector, error_vec);
                // }
            }
        }
        for e in error_vec.iter_mut() {
            *e /= BATCH_SIZE as f32;
        }
        //
        // loss / net.get_inputs_count() as f32 / BATCH_SIZE as f32,
        loss / size as f32 / net.get_inputs_count() as f32 / BATCH_SIZE as f32
    }

    pub fn predict_text(
        net: &mut Net,
        prompt: &Vec<u32>,
        to_predict: usize,
        hidden_state_size: usize,
    ) -> Vec<u32> {
        //
        net.prepare_for_next_sequence();
        //
        // Construct new inputs
        let mut outputs = vec![0.; net.get_outputs_count()];

        let mut output_vector = vec![];

        for v in prompt {
            clear_slice(&mut outputs);

            // Run predictions, just to advance the hidden state.
            net.evaluate(*v, &mut outputs);
        }
        // Actual prediction
        for _ in 0..to_predict {
            clear_slice(&mut outputs);

            // Run predictions, just to advance the hidden state.
            let input = if output_vector.len() > 0 {
                *output_vector.last().unwrap() as usize
            } else {
                let mut rng = WyRand::new();
                rng.generate_range(0..net.get_inputs_count())
            };
            net.evaluate(input as u32, &mut outputs);

            // Sample probabilistically.
            // We're squaring to decrease the odds of trash outputs.
            let mut total = 0.0;
            for value in &outputs {
                total += *value * *value;
            }

            let mut rng = WyRand::new();
            let threshold: f32 = rng.generate::<f32>() * total;
            let mut counter = 0.;
            let mut final_index = 0;
            for (index, value) in outputs.iter().enumerate() {
                counter += *value * *value;
                if counter >= threshold {
                    final_index = index;
                    break;
                }
            }

            output_vector.push(final_index as u32);
        }
        output_vector
    }
}

// Sets all elements of a slice to 0
fn clear_slice(slice: &mut [f32]) {
    for n in slice {
        *n = 0.;
    }
}
