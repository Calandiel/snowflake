use std::ops::MulAssign;

use rand_distr::{Distribution, Normal};
use serde::{Deserialize, Serialize};

use crate::{
    gpt::{zeros, Matrix},
    post_processing,
};

#[derive(Debug, Copy, Clone, Deserialize, Serialize)]
pub enum LayerType {
    Linear,
    Relu,
    Sigmoid,
    BinaryStep,
    Tanh,
    Softplus,
    Gaussian,
    LeakyRelu,
    Sin,
    RNN,
}
impl Default for LayerType {
    fn default() -> Self {
        LayerType::Linear
    }
}
//
// INPUT NEURONS
//
#[derive(Debug, Clone)]
pub struct NeuronLayer {
    pub weights: Matrix,
    pub neuron_type: LayerType,
    // RNN nonsense
    pub h: Option<Matrix>,
    pub w_hy: Option<Matrix>,
    pub w_hh: Option<Matrix>,
}
impl NeuronLayer {
    fn evaluate(&mut self, input: Matrix) -> Matrix {
        let mut input = input;
        input.mul_assign(&self.weights);
        let mut weighted = input;
        let nt = self.neuron_type;
        // hidden state updates
        match nt {
            LayerType::RNN => {
                //
                let mut new_hidden = self.h.as_ref().unwrap().clone();
                for (i, row) in self.w_hh.as_ref().unwrap().row_iter().enumerate() {
                    let dot_a = row.dot(&self.h.as_ref().unwrap());
                    new_hidden[(0, i)] = dot_a;
                }
                new_hidden += weighted;
                // apply tanh to the hidden state
                new_hidden.apply(|x| *x = (*x).tanh());
                self.h = Some(new_hidden);
                weighted = self.h.as_ref().unwrap() * self.w_hy.as_ref().unwrap();
            }
            _ => {}
        }
        // eval
        weighted.apply(|x| {
            *x = match nt {
                LayerType::Linear => *x,
                LayerType::Relu => (*x).max(0.),
                LayerType::Sigmoid => 1. / (1. + (-*x).exp()),
                LayerType::BinaryStep => {
                    if *x < 0. {
                        0.
                    } else {
                        1.0
                    }
                }
                LayerType::Tanh => {
                    let ex = (*x).exp();
                    let emx = (-*x).exp();
                    (ex - emx) / (ex + emx)
                }
                LayerType::Softplus => (1. + (*x).exp()).ln(),
                LayerType::Gaussian => (-*x).powi(2).exp(),
                LayerType::LeakyRelu => {
                    if *x < 0. {
                        0.01 * *x
                    } else {
                        *x
                    }
                }
                LayerType::Sin => (*x).sin(),
                LayerType::RNN => (*x).tanh(),
            }
        });
        weighted
    }

    pub fn typed(neuron_type: LayerType, previous_size: usize, my_size: usize) -> NeuronLayer {
        let n = NeuronLayer {
            weights: Matrix::zeros(previous_size, my_size),
            neuron_type,
            h: match neuron_type {
                LayerType::RNN => Some(zeros(1, my_size)),
                _ => None,
            },
            w_hh: match neuron_type {
                LayerType::RNN => Some(zeros(my_size, my_size)),
                _ => None,
            },
            w_hy: match neuron_type {
                LayerType::RNN => Some(zeros(my_size, my_size)),
                _ => None,
            },
        };
        n
    }
}
//
// NET
//
#[derive(Debug, Copy, Clone, Deserialize, Serialize)]
pub struct NetConfig {
    pub min_weight_magnitude: f32,
    pub min_firing_value: f32,
}
impl Default for NetConfig {
    fn default() -> Self {
        NetConfig {
            min_weight_magnitude: 0.,
            min_firing_value: f32::MIN, //0.01, // Minimum firing value for a neuron to "fire" to its "children". Set to f32::MIN to ALWAYS fire.. Note that it takes absolute value before making a check, so negative values will fire just fine
        }
    }
}
#[derive(Debug, Clone)]
pub struct Net {
    pub neurons: Vec<NeuronLayer>,
    pub embeddings: Matrix,
    input_neurons: usize,
    output_neurons: usize,
    pub seed: usize,
}
impl Net {
    // Creates a new network and initializes the inputs
    pub fn new(
        input_size: usize, // also embedding size
        output_size: usize,
        vocab_size: usize,
        rng_seed: usize,
    ) -> Net {
        let v: Vec<NeuronLayer> = Vec::with_capacity(10);

        Net {
            neurons: v,
            input_neurons: input_size,
            output_neurons: output_size,
            seed: rng_seed,
            embeddings: crate::gpt::zeros(vocab_size, input_size),
        }
    }

    // Adds a single neuron layer, not connected to anything.
    pub fn add_layer(&mut self, neuron: NeuronLayer) {
        self.neurons.push(neuron);
    }

    pub fn get_non_output_neuron_count(&self) -> usize {
        self.get_hidden_neuron_count() + self.input_neurons
    }

    // Returns the number of hidden neurons in the network
    pub fn get_hidden_neuron_count(&self) -> usize {
        self.neurons.len() - self.output_neurons - self.input_neurons
    }

    pub fn get_outputs_count(&self) -> usize {
        self.output_neurons
    }

    pub fn get_inputs_count(&self) -> usize {
        self.input_neurons
    }

    fn neuron_in_bounds(&self, id: usize) -> bool {
        self.neurons.len() > id
    }

    // Writes a networks weights to another network. Use it to limit deepcopies
    pub fn write_to(&self, other: &mut Net) {
        for n in self.neurons.iter().zip(other.neurons.iter_mut()) {
            *n.1 = n.0.clone();
        }
    }

    // Fills a neural network with random weights
    pub fn reseed(&mut self) {
        let mut rng = crate::annealing::RandWy::new();
        let normal = Normal::new(0.0, 1.0).unwrap();
        for n in &mut self.neurons {
            for r in &mut n.weights {
                *r = normal.sample(&mut rng);
            }
            if let Some(v) = &mut n.w_hh {
                for r in v {
                    *r = normal.sample(&mut rng);
                }
            }
            if let Some(v) = &mut n.w_hy {
                for r in v {
                    *r = normal.sample(&mut rng);
                }
            }
        }
        for e in &mut self.embeddings {
            *e = normal.sample(&mut rng);
        }
    }

    pub fn perturb(&mut self, learning_rate: f64) {
        let mut rng = crate::annealing::RandWy::new();
        let normal = Normal::new(0.0, 1.0).unwrap();
        for n in &mut self.neurons {
            for r in &mut n.weights {
                *r += (normal.sample(&mut rng) * learning_rate) as f32;
            }
            if let Some(v) = &mut n.w_hh {
                for r in v {
                    *r += (normal.sample(&mut rng) * learning_rate) as f32;
                }
            }
            if let Some(v) = &mut n.w_hy {
                for r in v {
                    *r += (normal.sample(&mut rng) * learning_rate) as f32;
                }
            }
        }
        for e in &mut self.embeddings {
            *e += (normal.sample(&mut rng) * learning_rate) as f32;
        }
    }

    // Wipes recursive state in preparation for the next sequence
    pub fn prepare_for_next_sequence(&mut self) {
        for n in &mut self.neurons {
            if let Some(h) = &mut n.h {
                h.apply(|f| *f = 0.);
            }
        }
    }

    // Evaluates the network and given inputs and writes outputs
    pub fn evaluate(&mut self, input: u32, outputs: &mut [f32]) {
        // create new inputs
        let inputs = self.embeddings.row(input as usize).transpose();
        let mut x: Matrix = Matrix::from_row_slice(1, self.input_neurons, inputs.as_slice());

        for layer in &mut self.neurons {
            x = layer.evaluate(x);
        }

        // Write outputs
        for o in 0..self.output_neurons {
            outputs[o] = x[(0, o)];
        }
        post_processing::softmax(outputs);
    }

    pub fn print_params(&self) {
        let mut edge_count = 0;
        for n in &self.neurons {
            edge_count += n.weights.len();
        }

        println!("Network parameters: {}", edge_count);
        println!("Network inputs: {}", self.input_neurons);
        println!("Network outputs: {}", self.output_neurons);
    }
}
