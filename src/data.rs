use std::path::PathBuf;

// Returns a byte vector from file (best used on text files...)
pub fn file_to_text_training_vector(file_path: PathBuf) -> Vec<u32> {
    if !crate::BERT {
        let tk = crate::tokenizer::Tokenizer::new();
        tk.string_to_tokens(&std::fs::read_to_string(file_path).unwrap())
    } else {
        let tokenizer =
            tokenizers::tokenizer::Tokenizer::from_pretrained(crate::TOKENIZER, None).unwrap();
        let encoding = tokenizer
            .encode(std::fs::read_to_string(file_path).unwrap(), false)
            .unwrap();
        encoding.get_ids().to_vec()
    }
}
