#[cfg(test)]
mod tests {

    use crate::gpt::ones;
    use crate::gpt::zeros;
    use crate::loss;

    use crate::gpt;

    #[test]
    fn testmat() {
        let mut mat1 = gpt::Matrix::zeros(2, 3);
        mat1[(0, 0)] = 2.;
        mat1[(0, 1)] = 100.;
        mat1[(1, 0)] = -5.;

        let mut mat1 = gpt::Matrix::from_row_slice(2, 3, &[2., 2., 3., -5., 0., 1.]);

        println!("before{}", mat1);
        gpt::layer_norm(&mut mat1, &ones(1, 3), &zeros(1, 3), None);
        println!("after{}", mat1);
    }
    /*
    #[test]
    fn test_loss() {
        let mut pred = vec![0.0f32; 10];
        let mut targ = vec![0.0f32; 10];

        assert!(loss::mean_square(&pred, &targ) == 0.);

        targ[0] = 1.;
        assert!(loss::mean_square(&pred, &targ) == 1. / 10.);

        pred[1] = 1.;
        assert!(loss::mean_square(&pred, &targ) == 2. / 10.);

        targ[0] = 0.;
        for p in &mut pred {
            *p = 1.;
        }
        assert!(loss::mean_square(&pred, &targ) == 1.);
    }
    */
}
