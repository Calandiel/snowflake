#[derive(Debug, Clone, Default)]
pub struct Tokenizer {
    //
    pub counter: u8,
    pub char_to_byte: std::collections::HashMap<char, u8>,
    pub byte_to_char: std::collections::HashMap<u8, char>,
}
impl Tokenizer {
    pub fn new() -> Tokenizer {
        let mut tk = Tokenizer::default();
        let vv = [
            ' ', '`', '~', '1', '!', '2', '@', '3', '#', '4', '$', '5', '%', '6', '^', '7', '&',
            '8', '*', '9', '(', '0', ')', '-', '_', '=', '+', 'q', 'Q', 'w', 'W', 'e', 'E', 'r',
            'R', 't', 'T', 'y', 'Y', 'u', 'U', 'i', 'I', 'o', 'O', 'p', 'P', '[', '{', ']', '}',
            '\\', '|', 'a', 'A', 's', 'S', 'd', 'D', 'f', 'F', 'g', 'G', 'h', 'H', 'j', 'J', 'k',
            'K', 'l', 'L', ';', ':', '\'', '"', 'z', 'Z', 'x', 'X', 'c', 'C', 'v', 'V', 'b', 'B',
            'n', 'N', 'm', 'M', ',', '<', '.', '>', '?', '/',
        ];
        for v in vv {
            tk.add(v);
        }
        tk
    }

    pub fn add(&mut self, c: char) {
        if self.char_to_byte.contains_key(&c) {
            return;
        }
        self.char_to_byte.insert(c, self.counter);
        self.byte_to_char.insert(self.counter, c);
        self.counter += 1;
    }

    fn get_byte(&self, c: char) -> u8 {
        if let Some(b) = self.char_to_byte.get(&c) {
            *b
        } else {
            0
        }
    }

    fn get_char(&self, b: u8) -> char {
        if let Some(c) = self.byte_to_char.get(&b) {
            *c
        } else {
            ' '
        }
    }

    pub fn string_to_tokens(&self, st: &String) -> Vec<u32> {
        let mut vv = vec![];
        for c in st.chars() {
            vv.push(self.get_byte(c) as u32);
        }
        vv
    }

    pub fn tokens_to_string(&self, v: &Vec<u32>) -> String {
        let mut st = String::default();
        for b in v {
            st.push(self.get_char(*b as u8));
        }
        st
    }
}
